import requests
from datetime import datetime
from typing import Any


class KalenteriParser:

    game_data_url: str = ""
    game_data: list[Any] = []
    events: list[str] = []

    def __init__(self, game_data_url):
        self.game_data_url = game_data_url

    def get_data(self):
        self.game_data = requests.get(self.game_data_url).json()

    def parse_events(self):
        for game in self.game_data:
            game_name = game["game"]
            game_category = game["category"]
            game_device = game["device"]
            game_published = game["published"]
            game_start_time = self._format_time(game["start_time"])
            game_end_time = self._format_time(game["end_time"])

            self.events.append(
                "BEGIN:VCALENDAR\n"
                "VERSION:2.0\n"
                "PRODID:-//ZContent.net//Zap Calendar 1.0//EN\n"
                "CALSCALE:GREGORIAN\n"
                "BEGIN:VEVENT\n"
                f"SUMMARY:{game_name}\n"
                f"UID:{game['id']}\n"
                f"DTSTAMP:{self._format_time(datetime.now().isoformat())}\n"
                f"DTSTART:{game_start_time}\n"
                f"DTEND:{game_end_time}\n"
                f"DESCRIPTION:{game_name} ({game_published}) {game_category} {game_device}\n"
                r"URL:https://www.twitch.tv/vauhtijuoksu" + "\n"
                "END:VEVENT\n"
                "END:VCALENDAR\n"
            )

    def write_ics_file(self):
        with open("vauhtijuoksu.ics", "w") as ics_file:
            for event in self.events:
                ics_file.writelines(event)

    def run(self):
        self.get_data()
        self.parse_events()
        self.write_ics_file()

    def _format_time(self, iso_time):
        return datetime.fromisoformat(iso_time).strftime("%Y%m%dT%H%M%SZ")


if __name__ == "__main__":
    kalenteri = KalenteriParser(r"https://api.dev.vauhtijuoksu.fi/gamedata")
    kalenteri.run()
